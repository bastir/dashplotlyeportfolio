import pandas as pd
import glob
import plotly.express as px  # (version 4.7.0)
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from datetime import datetime
import plotly.graph_objects as go
import numpy as np

#Read the Data
folderPath = ".\data"
li = []
for filename in glob.glob(folderPath + "\\*\\*.csv"):
    df = pd.read_csv(filename, encoding='UTF-8', sep=';',index_col=False)
    li.append(df)
frameAllData = pd.concat(li, axis=0, ignore_index=True)
app = dash.Dash(__name__)

#Reformat the Data
datedf = frameAllData
datedf = datedf.Timestamp.str.slice(stop = 10)
datedf= datedf.replace('.', '-')
datedf = pd.to_datetime(datedf)

frameAllData['VehID'] = frameAllData['VehID'].astype("string")
#Formating the Timestamp string into the Format Y-m-d H:M:S
frameAllData['Timestamp'] = frameAllData['Timestamp'].str.slice_replace(start=10, stop=11, repl=" ")
frameAllData['Timestamp'] = frameAllData['Timestamp'].str.replace(".", "-")
frameAllData['Timestamp'] = frameAllData['Timestamp'].str.slice_replace(start=19, stop=20, repl=".")

app.layout = html.Div([

    html.H1("QR code parking results - WBE",
            style={'text-align': 'center' , 'font-family' : 'Arial'}),
    html.Br(),
    html.Div([
        html.Div([
            html.Label(["Location ID",
            dcc.Dropdown(id='LocId',
                        options=[{'label': i, 'value':i} for i in frameAllData.LocID.unique()])])],
                        style={'font-family' : 'Arial','float':'left', 'display': 'inline', 'width': '317px', 'position' : 'relative', 'margin-left': '30px'}),
        html.Div([
            html.Label(["Vehicle ID",
            dcc.Dropdown(id='VehId',
                        options=[{'label': i, 'value':i} for i in frameAllData.VehID.unique()])])],
                        style={'font-family' : 'Arial','float':'left', 'display': 'inline', 'width':'317px','position' : 'relative', 'margin-left': '30px'}),
        dcc.DatePickerRange(
            id = 'DatePick',
            start_date_placeholder_text="Start Datum",
            end_date_placeholder_text="End Datum",
            calendar_orientation='vertical',
            clearable=True,
            min_date_allowed=datedf.min(),
            max_date_allowed= datedf.max()+ pd.DateOffset(1),
            style={'font-family': 'Arial', 'float': 'left', 'width': '600px', 'padding-top': '13px','left' : '30px'}
    )]),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Div([
        dcc.Graph(id='scatter_plot1',
                  figure=[],
                  style={'margin-top': '0px', 'display': 'table-row', 'float': 'left'}),

    ],
        style={'float': 'left', 'height' : '700px', 'width' : '1920px'})

])


@app.callback(
    [Output(component_id='scatter_plot1', component_property='figure')],
    [
        Input(component_id='LocId', component_property='value'),
        Input(component_id='VehId', component_property='value'),
        Input(component_id='DatePick', component_property='start_date'),
        Input(component_id='DatePick', component_property='end_date')
    ])
def update_bar(LcId, VhId, StartDate, EndDate):
    dff = pd.DataFrame(frameAllData)
    dff['Timestamp'] = pd.to_datetime(dff['Timestamp'])
    #Location Id Filter
    if LcId != None:
        dff = dff[dff['LocID'] == LcId]
    #Date Filter
    if StartDate != None:
        StartDate = datetime.strptime(StartDate, "%Y-%m-%d")
        dff = dff[dff['Timestamp'] >= StartDate]
    if EndDate != None:
        EndDate = datetime.strptime(EndDate, "%Y-%m-%d")
        dff = dff[dff['Timestamp'] <= EndDate]
    # Reformat the Time
    dff['Timestamp'] = dff['Timestamp'].dt.strftime('%d-%m-%Y %H:%M:%S')
    #Vehicle Id Filter
    if VhId != None:
        dff = dff[dff['VehID'] == str(VhId)]


    fig = px.scatter(dff,
                     x="QrPosZ",
                     y="QrPosX",
                     color="VehID",
                     hover_data=["Timestamp"])



    fig.update_xaxes(type="linear", range=(0, 600), fixedrange=False)
    fig.update_yaxes(type="linear", range=(-50, 50), fixedrange=False)


    fig.update_layout(
        autosize=True,
        width=500,
        height=500)

    return fig,

if __name__ == '__main__':
    app.run_server(debug=False)